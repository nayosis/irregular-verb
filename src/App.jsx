import "semantic-ui-css/semantic.min.css";
import { Container } from "semantic-ui-react";
import ExercicePage from "./pages/ExercicePage";
import { BrowserRouter, HashRouter, Route, Routes } from "react-router-dom";
import MainPage from "./pages/MainPage";
import LearnPage from "./pages/LearnPage";
import UserMainPage from "./pages/UserMainPage";
import AdminContext from "./context/AdminContext";

function App() {
  return (
    <>
      <AdminContext.Provider value={false}>
        <Container>
          <HashRouter>
            <div>
              {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
              <Routes>
                <Route
                  path="/exercice/:exerId"
                  element={<ExercicePage />}
                ></Route>
                <Route
                  path="/owner/:owner/exercice/:exerId"
                  element={<ExercicePage />}
                ></Route>
                <Route path="/owner/:owner" element={<UserMainPage />}></Route>
                <Route path="/learn" element={<LearnPage />}></Route>
                <Route path="/" element={<MainPage />}></Route>
              </Routes>
            </div>
          </HashRouter>
        </Container>
      </AdminContext.Provider>
    </>
  );
}

export default App;
