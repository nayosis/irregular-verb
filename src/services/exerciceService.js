import { v4 as uuidv4 } from "uuid";

import { getRandom, groupVerbOption } from "../utils/verbeConfig";

export function createQuestionnaire(
  owner,
  groups,
  questionNumber,
  options = {}
) {
  // options
  //"infinitive"
  //"simplePast"
  //"pastParticiple"
  //"french"
  // none => formDisplayed = ["Infinitive"] "simplePast" "pastParticiple" "french"
  if (
    options.formsDisplayed === undefined ||
    options.formsDisplayed.length === 0
  ) {
    options.formsDisplayed = ["infinitive"];
  }

  let definedgroup = [];
  if (groups.length === 0) {
    definedgroup = groupVerbOption.map((grpVerb) => grpVerb.value);
  } else {
    definedgroup = groups;
  }
  const verbs = getRandom(definedgroup, questionNumber).map((innerVerb) => {
    return {
      ...innerVerb,
      formDisplayed: getRandomOne(options.formsDisplayed),
    };
  });
  const uuid = uuidv4();

  console.log("options", options);
  const questionnaire = {
    id: uuid,
    owner: owner,
    verbs: verbs,
    groups: definedgroup,
    questionNumber,
    options: options,
    etape: 0,
    date: Date.now(),
  };
  localStorage.setItem(`quest-${uuid}`, JSON.stringify(questionnaire));
  return questionnaire;
}

export function saveQuestionnaire(uuid, verbs, etape) {
  const working = readQuestionnaire(`${uuid}`);

  const questionnaire = {
    ...working,
    verbs: verbs,
    etape: etape,
    lastModif: Date.now(),
  };
  localStorage.setItem(`quest-${uuid}`, JSON.stringify(questionnaire));
}

export function readQuestionnaire(uuid) {
  return JSON.parse(localStorage.getItem(`quest-${uuid}`));
}

export function listQuestionnaires() {
  for (let i = 0; i < localStorage.length; i++) {
    console.log(localStorage.key(i));
  }
}

export function deleteQuestionnaire(uuid) {
  console.log("delete", uuid);
  localStorage.removeItem(`quest-${uuid}`);
}

export function listQuestionnaire() {
  let list = [];
  for (let i = 0; i < localStorage.length; i++) {
    if (localStorage.key(i).includes(`quest-`)) {
      const texxxt = readQuestionnaire(
        localStorage.key(i).replace("quest-", "")
      );
      list.push(texxxt);
    }
  }
  return list.sort(comparedate);
}

function comparedate(a, b) {
  return b.date - a.date;
}

function getRandomOne(list) {
  const shuffled = list.sort(() => 0.5 - Math.random());
  return shuffled[0];
}
