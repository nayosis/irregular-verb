import {
  Button,
  Flag,
  Form,
  FormGroup,
  FormInput,
  Header,
  HeaderContent,
  Icon,
  Message,
  Segment,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from "semantic-ui-react";

import { Link } from "react-router-dom";
import { useState } from "react";
import { listQuestionnaire } from "../services/exerciceService";

const MainPage = () => {
  const [questionnaires, setQuestionnaires] = useState(listQuestionnaire());
  const [owner, setOwner] = useState("");
  const listeUser = getUniqueValues(
    questionnaires?.map((innerQuest) => innerQuest.owner)
  );

  return (
    <>
      <div>
        <Header as="h2" textAlign="center" block style={{ marginTop: "1rem" }}>
          <HeaderContent>
            <Flag name="gb" /> Test de connaissance des verbes irréguliers
            Anglais (4ieme)
            <Flag name="gb" />
          </HeaderContent>
        </Header>
        <Message info>
          <Icon name="bullhorn" />
          Cette application permet de tester sa connaissance des verbes
          irréguliers. Si vous êtes nouveau, remplissez votre nom et clique sur
          acceder à votre page. Si vous êtes lister dans la liste, cliquez qur
          votre nom.
        </Message>
        <Segment>
          <Form>
              <label>Taper votre nom. (3 caractères minimum)</label>
              <FormInput
                placeholder="Nom ou prénom"
                value={owner}
                onChange={(e, d) => setOwner(d.value)}
                required={true}
              />
            <Button
              as={Link}
              to={`/owner/${owner}`}
              disabled={owner.length < 3}
            >
              {" "}
              acceder à votre page ({owner})
            </Button>
          </Form>
        </Segment>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderCell width={14}>Nom</TableHeaderCell>
            </TableRow>
          </TableHeader>
          <TableBody>
            {listeUser?.map((user) => {
              return (
                <TableRow key={user}>
                  <TableCell>
                    {" "}
                    <Link to={`/owner/${user}`}> {user} </Link>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
      <Message
        icon="info"
        header="Informations"
        content={
          <>
            {" "}
            <p>
              L&apos;ensemble des données ( nom, username, exercices) que vous
              saisissez dans cette application sont stockées dans votre
              navigateur sur votre poste. Aucune donnée n&apos;est récupéré sur
              le serveur.
            </p>
            <p>
              Cette application a été développé (rapidement) pour un collégien
              en retard sur ces verbes irréguliers.{" "}
              <Link to="/learn">Les verbes configurés </Link> sont ceux dont il
              avait besoin. Les sources sont disponible sur{" "}
              <a
                href="https://gitlab.com/nayosis/irregular-verb"
                target="_blank"
                rel="noreferrer"
              >
                <Icon name="gitlab" /> Gitlab
              </a>
            </p>
          </>
        }
      />
    </>
  );
};

export default MainPage;

const getUniqueValues = (array) =>
  array.filter(
    (currentValue, index, arr) => arr.indexOf(currentValue) === index
  );
