import {
  Button,
  Divider,
  Flag,
  Form,
  FormDropdown,
  FormGroup,
  FormInput,
  Header,
  HeaderContent,
  Icon,
  Label,
  Message,
  Modal,
  ModalActions,
  ModalContent,
  ModalDescription,
  ModalHeader,
  Popup,
  Segment,
  Tab,
  TabPane,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from "semantic-ui-react";

import { Link, useParams } from "react-router-dom";
import React, { useContext, useState } from "react";
import { groupVerbOption, optionsFormDisplayeds } from "../utils/verbeConfig";
import {
  createQuestionnaire,
  deleteQuestionnaire,
  listQuestionnaire,
} from "../services/exerciceService";
import BarStatus from "../comp/BarStatus";
import { calculScore } from "../utils/exerciceUtil";
import Suivi from "../comp/graph/Suivi";
import AdminContext from "../context/AdminContext";

const optionsCount = [
  {
    key: "5",
    text: "5",
    value: "5",
  },
  {
    key: "10",
    text: "10",
    value: "10",
  },
  {
    key: "15",
    text: "15",
    value: "15",
  },
  {
    key: "20",
    text: "20",
    value: "20",
  },
];

const UserMainPage = () => {
  let params = useParams();
  const isAdmin = useContext(AdminContext);
  const [questionnaires, setQuestionnaires] = useState(listQuestionnaire());
  const owner = params.owner;
  const [nbr, setNbr] = useState("5");
  const [grpVerbe, setGrpVerbe] = useState([]);
  const [options, setOptions] = useState({ formsDisplayed: ["infinitive"] });

  function handleCreateQuestionnaire(owner, grpVerbe, nbr, options) {
    createQuestionnaire(owner, grpVerbe, nbr, options);
    setQuestionnaires(listQuestionnaire());
  }

  function updateOption(type, value) {
    let newOptions = { ...options };
    newOptions[type] = value;
    setOptions(newOptions);
  }
  const panelData = buildGraphPanel(
    questionnaires?.filter((questionnaire) => questionnaire.owner === owner)
  );

  return (
    <>
      <div>
        <Header as="h2" textAlign="center" block style={{ marginTop: "1rem" }}>
          <HeaderContent>
            <Flag name="gb" /> Test de connaissance des verbes irréguliers
            Anglais (4ieme)
            <Flag name="gb" />
          </HeaderContent>
        </Header>
        <Message info>
          <Icon name="bullhorn" />
          Cette application permet de tester sa connaissance des verbes
          irréguliers. Remplissez votre nom , le nombre de verbes, et leurs
          catégories.
        </Message>
        <Link to="/" as={Button} icon labelPosition="left">
          <Icon name="left arrow" />
          Retour
        </Link>
        <Segment>
          <Form>
            <label>Selectionner le nombre de verbe que vous désirez :</label>
            <FormDropdown
              placeholder="nbr"
              value={nbr}
              onChange={(e, d) => setNbr(d.value)}
              selection
              options={optionsCount}
            />
            <label>
              Selectionner les groupes de verbes (si aucun n&apos;est
              sélectionner, vous serez interrogé sur l&apos;ensemble):
            </label>
            <FormDropdown
              placeholder="groupe de verbe"
              value={grpVerbe}
              fluid
              onChange={(e, d) => setGrpVerbe(d.value)}
              multiple
              selection
              options={groupVerbOption}
            />
            <label>
              Sélectionner à partir de quelles formes vous souhaitez être
              interroger ( Infinitif, Simple Past){" "}
              <Popup
                content="Si vous en sélectionnez plusieurs, ils seront tirés aléatoirement"
                trigger={<Label circular>?</Label>}
              />
            </label>

            <FormDropdown
              placeholder="Form"
              value={options.formsDisplayed}
              fluid
              onChange={(e, d) => updateOption("formsDisplayed", d.value)}
              multiple
              selection
              options={optionsFormDisplayeds}
            />
            <Button
              fluid
              disabled={isEmpty(owner)}
              onClick={() =>
                handleCreateQuestionnaire(owner, grpVerbe, nbr, options)
              }
            >
              {" "}
              creer questionnaire{" "}
            </Button>
          </Form>
        </Segment>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderCell width={3}>Nom et Date</TableHeaderCell>
              <TableHeaderCell width={7}>
                <Icon name="tags" />
                Groupe de verbe
              </TableHeaderCell>
              <TableHeaderCell width={3}>Score</TableHeaderCell>
              <TableHeaderCell width={1}></TableHeaderCell>
            </TableRow>
          </TableHeader>
          <TableBody>
            {listQuestionnaire()
              ?.filter((questionnaire) => questionnaire.owner === owner)
              .map((questionnaire) => {
                return (
                  <TableRow
                    key={questionnaire.id}
                    active={questionnaire.etape === questionnaire.verbs.length}
                  >
                    <TableCell>
                      {questionnaire.owner}
                      <br />
                      {questionnaire.lastModif &&
                        new Date(questionnaire.lastModif).toLocaleDateString() +
                          " " +
                          new Date(
                            questionnaire.lastModif
                          ).toLocaleTimeString()}
                      <br />
                    </TableCell>
                    <TableCell>
                      <div>
                        <span>groupe(s) concerné(s) :</span>
                        {questionnaire.groups.length !== 0
                          ? questionnaire.groups
                              .map((grp) =>
                                groupVerbOption.find(
                                  (grpVerOpt) => grpVerOpt.value === grp
                                )
                              )
                              .map((grp) => (
                                <Label key={grp.value}>{grp.text}</Label>
                              ))
                          : "tous"}
                      </div>
                      <Divider />
                      <div>
                        <span>Enoncé par :</span>
                        {questionnaire.options?.formsDisplayed?.map(
                          (innerForm) => (
                            <Label key={innerForm}>
                              {
                                optionsFormDisplayeds.find(
                                  (xxx) => xxx.key === innerForm
                                ).text
                              }
                            </Label>
                          )
                        )}
                      </div>
                    </TableCell>
                    <TableCell>
                      <BarStatus questionnaire={questionnaire} />
                    </TableCell>
                    <TableCell>
                      <Button
                        size="tiny"
                        as={Link}
                        color={
                          questionnaire.etape !== questionnaire.verbs.length
                            ? "olive"
                            : "blue"
                        }
                        to={`/exercice/${questionnaire.id}`}
                      >
                        {questionnaire.etape !== questionnaire.verbs.length
                          ? "Acceder"
                          : "Résultat"}
                      </Button>
                      {isAdmin && (
                        <>
                          {" "}
                          <Button
                            onClick={() =>
                              deleteQuestionnaire(questionnaire.id)
                            }
                          >
                            Delete
                          </Button>
                          <ModalView questionnaire={questionnaire} />
                        </>
                      )}
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </div>
      <Tab menu={{ secondary: true, pointing: true }} panes={panelData} />
    </>
  );
};

export default UserMainPage;

function isEmpty(value) {
  return (
    value == null || (typeof value === "string" && value.trim().length === 0)
  );
}

function buildData(questionnaires, name) {
  const data = questionnaires
    .filter((innerQuest) => innerQuest.owner === name)
    .filter((innerQuest) => innerQuest.etape === innerQuest.verbs.length)
    .reverse()
    .map((innerQuest) => {
      const info = calculScore(innerQuest.verbs);
      return {
        x: new Date(innerQuest.date),
        y: (info.score / info.total) * 100,
      };
    });

  return [
    {
      id: name,
      color: "hsl(104, 70%, 50%)",
      data: data,
    },
  ];
}

const getUniqueValues = (array) =>
  array.filter(
    (currentValue, index, arr) => arr.indexOf(currentValue) === index
  );

function buildGraphPanel(questionnaires) {
  return getUniqueValues(
    questionnaires?.map((innerQuest) => innerQuest.owner)
  ).map((owner) => {
    return {
      menuItem: owner,
      render: () => (
        <TabPane>
          <div key={owner} style={{ height: "50vh" }}>
            <Suivi data={buildData(questionnaires, owner)} />
          </div>
        </TabPane>
      ),
    };
  });
}

const ModalView = ({ questionnaire }) => {
  const [open, setOpen] = React.useState(false);

  return (
    <Modal
      centered={false}
      open={open}
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      trigger={<Button>Information</Button>}
    >
      <ModalHeader>Thank you!</ModalHeader>
      <ModalContent>
        <ModalDescription>
          <pre>{JSON.stringify(questionnaire, null, 2)}</pre>
        </ModalDescription>
      </ModalContent>
      <ModalActions>
        <Button onClick={() => setOpen(false)}>OK</Button>
      </ModalActions>
    </Modal>
  );
};
