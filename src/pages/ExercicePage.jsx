import {
  Button,
  Card,
  CardContent,
  CardDescription,
  CardGroup,
  StatisticValue,
  StatisticLabel,
  Statistic,
  Divider,
  Form,
  FormGroup,
  FormInput,
  Grid,
  GridColumn,
  Header,
  Icon,
  Segment,
  FormField,
  Input,
} from "semantic-ui-react";
import "./exercice.scss";

import React, { useState } from "react";
import {
  readQuestionnaire,
  saveQuestionnaire,
} from "../services/exerciceService";
import { calculScore, isCorrect } from "../utils/exerciceUtil";
import { Link, useParams, useSearchParams } from "react-router-dom";
import BarStatus from "../comp/BarStatus";

const ExercicePage = () => {
  let params = useParams();
  const thisRef = React.useRef(null);
  const [searchParams] = useSearchParams();
  const isTraining = searchParams.get("training");
  const questionnaire = readQuestionnaire(params.exerId);
  const [uuid, setUuid] = useState(questionnaire.id);
  const [verbs, setVerbs] = useState(questionnaire.verbs);
  const [etape, setEtape] = useState(questionnaire.etape);

  const [infinitive, setInfinitive] = useState("");
  const [simplePast, setSimplePast] = useState("");
  const [pastParticiple, setPastParticiple] = useState("");
  const [french, setFrench] = useState("");

  function validate(number, innerInf, innerSP, innerPP, innerFr) {
    verbs[number].response = {
      inf:
        verbs[number].formDisplayed === "infinitive"
          ? verbs[number].infinitive
          : innerInf,
      sp:
        verbs[number].formDisplayed === "simplePast"
          ? verbs[number].simplePast
          : innerSP,
      pp:
        verbs[number].formDisplayed === "pastParticiple"
          ? verbs[number].pastParticiple
          : innerPP,
      fr:
        verbs[number].formDisplayed === "french"
          ? verbs[number].french
          : innerFr,
    };

    saveQuestionnaire(uuid, [...verbs], number + 1);

    setVerbs([...verbs]);
    setInfinitive("");
    setSimplePast("");
    setPastParticiple("");
    setFrench("");
    setEtape(number + 1);

    //thisRef.current.focus();
  }

  if (etape + 1 > verbs.length) {
    return <FinalResult questionnaire={questionnaire} debug={false} />;
  }

  return (
    <>
      <div className="exercice-page">
        <div className="exercice-titre">
          <div>
            <Header as="h2">Question {etape + 1}</Header>
            <BarStatus questionnaire={questionnaire} />
            <Button
              to={`/owner/${questionnaire.owner}`}
              as={Link}
              icon
              labelPosition="left"
            >
              <Icon name="left arrow" />
              Retour
            </Button>
          </div>
        </div>
        <div className="exercice-response">
          <div>
            <Form>
              <FormGroup widths="equal" style={{ fontSize: "1.3rem" }}>
                <FormField>
                  <label>Infinitive</label>
                  {questionnaire.verbs[etape].formDisplayed === undefined ||
                  questionnaire.verbs[etape].formDisplayed === "infinitive" ? (
                    <MyLabel label={questionnaire.verbs[etape].infinitive} />
                  ) : (
                    <Input
                      fluid
                      value={infinitive}
                      onChange={(e, d) => setInfinitive(d.value)}
                      placeholder="Infinitive"
                    />
                  )}
                </FormField>
                <FormField>
                  <label>Simple Past</label>
                  {questionnaire.verbs[etape].formDisplayed === "simplePast" ? (
                    <MyLabel label={questionnaire.verbs[etape].simplePast} />
                  ) : (
                    <Input
                      fluid
                      value={simplePast}
                      onChange={(e, d) => setSimplePast(d.value)}
                      placeholder="Simple Past"
                    />
                  )}
                </FormField>
                <FormField>
                  <label>Past Participate</label>
                  {questionnaire.verbs[etape].formDisplayed ===
                  "pastParticiple" ? (
                    <MyLabel
                      label={questionnaire.verbs[etape].pastParticiple}
                    />
                  ) : (
                    <Input
                      fluid
                      value={pastParticiple}
                      onChange={(e, d) => setPastParticiple(d.value)}
                      placeholder="Past Participate"
                    />
                  )}
                </FormField>
                <FormField>
                  <label>French</label>
                  {questionnaire.verbs[etape].formDisplayed === "french" ? (
                    <MyLabel label={questionnaire.verbs[etape].french} />
                  ) : (
                    <Input
                      fluid
                      value={french}
                      onChange={(e, d) => setFrench(d.value)}
                      placeholder="Past Participate"
                    />
                  )}
                </FormField>
              </FormGroup>
            </Form>
            <Button
              floated="right"
              icon
              labelPosition="right"
              onClick={() =>
                validate(etape, infinitive, simplePast, pastParticiple, french)
              }
            >
              Next
              <Icon name="right arrow" />
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ExercicePage;

function FinalResult({ questionnaire, debug }) {
  const grpScore = calculScore(questionnaire.verbs);
  let totalScore = grpScore.score;

  return (
    <>
      <div>
        <Header as="h2">Résultat </Header>
        <BarStatus questionnaire={questionnaire} />
        <Button
          to={`/owner/${questionnaire.owner}`}
          as={Link}
          icon
          labelPosition="left"
        >
          <Icon name="left arrow" />
          Retour
        </Button>
      </div>
      <Segment textAlign="center">
        <Statistic>
          <StatisticValue>
            {totalScore}/{grpScore.total}
          </StatisticValue>
          <StatisticLabel>Score</StatisticLabel>
        </Statistic>
      </Segment>
      <CardGroup itemsPerRow={3} stackable>
        {questionnaire.verbs?.map((result) => {
          return (
            <Card key={result.infinitive}>
              <CardContent>
                <CardDescription>
                  <Comparator
                    estLEnnonce={
                      result.formDisplayed === "infinitive" ||
                      result.formDisplayed === undefined
                    }
                    correction={result.infinitive}
                    reponse={result.response.inf}
                  />
                  <Comparator
                    estLEnnonce={result.formDisplayed === "simplePast"}
                    correction={result.simplePast}
                    reponse={result.response.sp}
                  />
                  <Comparator
                    estLEnnonce={result.formDisplayed === "pastParticiple"}
                    correction={result.pastParticiple}
                    reponse={result.response.pp}
                  />
                  <Comparator
                    estLEnnonce={result.formDisplayed === "french"}
                    correction={result.french}
                    reponse={result.response.fr}
                  />
                </CardDescription>
              </CardContent>
              <CardContent extra>{result.commentaire}</CardContent>
            </Card>
          );
        })}
      </CardGroup>
      {debug && <pre>{JSON.stringify(data, null, 4)}</pre>}
    </>
  );
}

const Check = (a, b) => {
  if (isCorrect(a, b)) {
    return <Icon color="green" name="check circle" />;
  } else {
    return <Icon color="red" name="times circle" />;
  }
};

const Comparator = ({ estLEnnonce, correction, reponse }) => {
  if (estLEnnonce) {
    return (
      <Segment tertiary inverted color="olive" textAlign="center">
        {correction?.toUpperCase()}
      </Segment>
    );
  }

  return (
    <Segment color={isCorrect(reponse, correction) ? "green" : "red"}>
      <Grid columns={2} relaxed="very" stackable>
        <GridColumn verticalAlign="middle">
          {correction?.toUpperCase()}
        </GridColumn>

        <GridColumn verticalAlign="middle">
          {reponse?.toUpperCase()}{" "}
        </GridColumn>
      </Grid>

      <Divider vertical> {Check(reponse, correction)}</Divider>
    </Segment>
  );
};

const MyLabel = ({ label }) => {
  return (
    <Segment
      color="grey"
      inverted
      tertiary
      style={{
        fontSize: "1.3rem",
        padding: "0.7em 1em",
        marginTop: "0",
      }}
    >
      {label}
    </Segment>
  );
};
